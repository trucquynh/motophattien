<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Illuminate\Database\Capsule\Manager as DB;


class CustomAdminController extends adminController
{

    public function exportProduct($skip, $take, $name)
    {
        $products = Product::join('variant', 'product.id', '=', 'variant.product_id')
            ->where('product.status', '!=', 'delete')
            ->groupBy('variant.id')
            ->orderBy('product.id', "asc")
            ->select('product.*', 'variant.title as variant_title', 'variant.id as variant_id', 'variant.price as price', 'variant.price_compare as price_compare', 'variant.stock_quant as stock_quant', 'variant.sku_code as sku_code', 'variant.weight as weight')
            ->skip($skip)->take($take)->get();
        $result = array(["Mã sản phẩm", "Mã variant", "Tên sản phẩm", "Tên phiên bản", "Giá bán", "Giá so sánh", "Tồn Kho", "Nhóm sản phẩm"]);
        $Attribute = Attributes();
        foreach ($products as $key => $value) {
            $collections = Product::join('collection_product', 'product.id', '=', 'collection_product.product_id')
                ->join('collection', 'collection_product.collection_id', '=', 'collection.id')
                ->where('collection_product.product_id', $value->id)
                ->where('collection.status', '!=', 'delete')
                ->select('collection.id', 'collection.title')
                ->pluck('collection.title')->toArray();
            $collections = implode(',', $collections);
            $column = array($value->id,
                $value->variant_id,
                $value->title,
                $value->variant_title,
                money($value->price) . 'đ',
                money($value->price_compare) . 'đ',
                $value->stock_quant,
                $collections
            );
            array_push($result, $column);
            error_log("test-id: " . $value->id);
        };
        $url = $this->exportExcelProductVL($result, $name, $Attribute);
        return $url;
    }

    function setDataValidationList($objPHPExcel, $column, $value)
    {
        // error_log("columns: " . $column . " value: ".$value);
        if ($column == 'H') {
            return 0;
        }
        $objPHPExcel->setActiveSheetIndex(0);

        for ($i = 2; $i <= $objPHPExcel->getActiveSheet()->getHighestRow(); $i++) {
            // error_log("test: ".$i);
            $objValidation = $objPHPExcel->getActiveSheet()->getCell($column . $i)->getDataValidation();
            $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1($value);    // Make sure to put the list items between " and " if your list is simply a comma-separated list of values !!!
        }
    }

    function exportExcelProductVL($data, $name, $attribute)
    {
        $objPHPExcel = new PHPExcel;
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objPHPExcel->setActiveSheetIndex(0);
        $objSheet = $objPHPExcel->getActiveSheet();

        $row = 1;
        foreach ($data as $key => $item) {
            $col = 0;
            for ($i = 0; $i < count($item); $i++) {
                $objSheet->setCellValueByColumnAndRow($col, $row, $item[$i]);
                $col++;
            }
            $row++;
        }

        for ($i = 'A'; $i <= $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
        }
        $s = 'G';
        $y = 'A'; // Collum attributes start
        $start_r = 1; // Cell attribute start

        // error_log("Attribute: ".json_encode($attribute));
        foreach ($attribute as $key => $value) {
            $count = count($value->child);
            $value = "datavalidation!$" . $y . "$" . $start_r . ":$" . $y . "$" . $count;
            CustomAdminController::setDataValidationList($objPHPExcel, $s, $value);
            $s++;
            $y++;
        }
        // setDataValidation($objPHPExcel,'N' ,'Đang phát hành,Sắp phát hành,Sắp có hàng');


        // Add new sheet
        $objWork = $objPHPExcel->createSheet(1);
        $objWork->setTitle('datavalidation');
        $objPHPExcel->setActiveSheetIndex(1);

        $y = 'A'; // Collum attributes start

        $status_product = array('Đang phát hành', 'Sắp phát hành', 'Sắp có hàng');

        foreach ($attribute as $key => $value) {
            foreach ($value->child as $key => $item) {
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue($y . ($key + $start_r), $item);
            }
            $y++;
        }
        foreach ($status_product as $key => $item) {
            $objPHPExcel->getActiveSheet()->setCellValue($y . ($key + $start_r), $item);
        }

        $value = "datavalidation!$" . $y . "$" . $start_r . ":$" . $y . "$" . count($status_product);
        $objPHPExcel->setActiveSheetIndex(0);

        if (!is_dir(ROOT . '/public/static/excel')) {
            mkdir(ROOT . '/public/static/excel');
        }

        $file_path = ROOT . '/public/static/excel/';
        $file_name = $name . '.xlsx';

        $objWriter->save($file_path . $file_name);

        return HOST . '/static/excel/' . $file_name;
    }


    public function importProduct(Request $req, Response $res)
    {
        try {
            $tmp_name = $_FILES['file']['tmp_name'];
            $new_name = 'import_' . time() . '.xlsx';
            $path = ROOT . '/public/static/excel/' . $new_name;
            $moveFileSuccess = move_uploaded_file($tmp_name, $path);

            if (!$moveFileSuccess) {
                return $res->withJson([
                    'success' => false,
                    'message' => 'error'
                ]);
            }

            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);

            $objPHPExcel = $objReader->load($path);
            $worksheet = $objPHPExcel->getSheet(0);
            $highestRow = $worksheet->getHighestRow();
            $lastColumn = $worksheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; $row++) {
                $list_MF = array();
                $variantObj = array();
                $variantObj = $this->parseProduct($worksheet, $row, $lastColumn);
                if ($variantObj["id"]) {
                    $variant_id = Variant::updateItem($variantObj['id'], $variantObj);
                    if (!$variant_id) {
                        error_log("========: ");
                        # code...
                    }
                }
            }
            return $res->withJson([
                'success' => true,
                'message' => 'success'
            ]);

        } catch (Exception $e) {
            return $res->withJson([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }

    public function parseProduct($worksheet, $row, $lastColumn)
    {
        $countColumn = 0;
        $variantObj = array();
        $lastColumn++;
        $column = 'A';
        $column--;
        do {
            $countColumn++;
            $title_column = $worksheet->getCell($column . '1')->getValue();
            $value = $worksheet->getCell($column . $row)->getValue();
            $column++;
            switch ($title_column) {
                case 'Mã variant':
                    $variantObj['id'] = $value;
                    break;
                case 'Khối lượng':
                    $variantObj['weight'] = $value;
                    break;
                case 'Giá bán':
                    $variantObj['price'] = $this->convertMoneyToString($value);
                    break;
                case 'Giá so sánh':
                    $variantObj['price_compare'] = $this->convertMoneyToString($value);
                    break;
                case 'Tồn kho':
                    $variantObj['stock_quant'] = $value;
                    break;
            }
        } while ($column != $lastColumn);
        return $variantObj;
    }

    function convertMoneyToString($str_money)
    {
        return preg_replace('/[^0-9]/', '', $str_money);
    }

    function getColor(Request $request, Response $response)
    {
        $color = getCustomField(1, 'color', 'color');
        if ($color) {
            return $response->withJson([
                "code" => 0,
                "color" => $color
            ]);
        }
        return $response->withJson([
            "code" => -1,
            "color" => $color
        ]);
    }

    public function updateColorAttribute(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $code = Metafield::store($body);
        $result = ControllerHelper::response($code);
        return $response->withJson($result, 200);
    }

    public function createColorAttribute(Request $request, Response $response)
    {
        $color = getCustomField(1, 'color', 'color');
        $attributes = Attribute::where('parent_id', '1')->where('status', 'active')->get();
        return $this->view->render($response, 'admin/color-setting', [
            'color' => $color,
            'attributes' => $attributes
        ]);
    }

    public function listOrder(Request $request, Response $response)
    {
        $params = $request->getQueryParams();
        $order_status = $params['order_status'];
        $payment_status = $params['payment_status'];
        $orderType = $params['type'];
        $menu_child = 'all';
        $statistic = [];

        if (isset($payment_status) && $payment_status == 1) $menu_child = 'paid';
        $query = Order::join('customer', 'customer.id', '=', 'order.customer_id')
            ->leftJoin('shipping_address', 'shipping_address.order_id', '=', 'order.id')
            ->leftJoin('region', 'shipping_address.region', '=', 'region.id')
            ->leftJoin('subregion', 'shipping_address.subregion', '=', 'subregion.id');

        $query = $query->where('order.order_status', '!=', 'delete');

        if (isset($order_status)) {
            $menu_child = $order_status;
            $query = $query->where('order.order_status', $order_status);
            if (isset($payment_status)) {
                $query = $query->where('order.payment_status', $payment_status);
                if ($payment_status == 0) $menu_child = 'unpaid';
                else $menu_child = 'paid';
            }
        }

        $arrTypeAlow = array('normal', 'instalment');

        if ($orderType && in_array($orderType, $arrTypeAlow)) {
            $query = $query->where('order.notes', $orderType);
            if ($orderType == 'instalment') {
                $menu_child = 'instalment';
            }
        }

        $statistic['totalSum'] = $query->sum('order.total');
        $statistic['orderCount'] = $query->count();

        $data = $query->select(
            'order.*',
            'customer.name as customer_name',
            'region.name as region_name',
            'subregion.name as subregion_name',
            'shipping_address.name as shipping_name',
            'shipping_address.email as shipping_email',
            'shipping_address.address as shipping_address',
            'shipping_address.phone as shipping_phone',
            'shipping_address.region as shipping_region',
            'shipping_address.subregion as shipping_subregion'
        )->orderBy('order.id', 'desc')->get();


        foreach ($data as $key => $order) {
            $order->sale = json_decode($order->sale);
        }

        return $this->view->render($response, 'admin/order/list', array(
            'data' => $data,
            'menu_child' => $menu_child,
            'statistic' => $statistic
        ));
    }

    public function statusOrder(Request $request, Response $response)
    {
        $params = $request->getQueryParams();
        $status = $params['status'];
        if ($status) {
            $count = Order::where('notes', $status)->count();
        }
        return $response->withJson([
            'code' => 0,
            'count' => $count
        ]);
    }


    public function importCollections()
    {
      $collection_path = dirname(__FILE__) . '/import/collections.json';
      $collections = file_get_contents($collection_path);
      $collections = json_decode($collections, true);

      foreach ($collections as $collection) {
        $slug = $collection['slug'];
        $item = Collection::create([
          "title" => $collection['title'],
          "parent_id" => -1,
          "description" => '',
          "content" => '',
          "image" => $collection['photo'],
          "status" => 'active',
          "template" => '',
          "tags" => '',
          "created_at" => date('Y-m-d H:i:s'),
          "updated_at" => date('Y-m-d H:i:s')
        ]);
        $collection_id = $item->id;
        Slug::store($collection_id, "collection", $slug);
      }

    }

    public function importProducts()
    {
      $product_path = dirname(__FILE__) . '/import/products.json';
      $photo_path = dirname(__FILE__) . '/import/photos.json';

      $products = file_get_contents($product_path);
      $products = json_decode($products, true);

      $photos = file_get_contents($photo_path);
      $photos = json_decode($photos, true);

      $arr_photos = [];
      foreach ($photos as $photo) {
        $arr_photos[$photo['slug']][] = $photo['photo'];
      }

      foreach ($products as $product) {
        $item = new Product;
        $item->title = $product['title'];
        $item->image = $product['photo'] ?: '';
        $item->description = $product['description'] ?: '';
        $item->content = $product['content'] ?: '';
        $item->stock_quant = 0;
        $item->stock_manage = 0;
        $item->stop_selling = 'publish';
        $item->view = $product['view'];
        $item->sell = 0;
        $item->status = 'active';
        $item->priority = 1000;

        $item->option_1 = '';
        $item->option_2 = '';
        $item->option_3 = '';
        $item->option_4 = '';
        $item->option_5 = '';
        $item->option_6 = '';
        $item->tags = '';
        $item->template = '';

        $item->raw_title = parse_raw_title($item);
        $item->raw_full = parse_raw_product($item);

        $item->created_at = date('Y-m-d H:i:s');
        $item->updated_at = date('Y-m-d H:i:s');
        $item->save();

        $handle = $product['slug'];

        $product_id = $item->id;
        $sku = $product['sku'];
        $price = $product['price'];
        $collection_id = $product['collection_id'];
        $photos = $arr_photos[$handle];

        Slug::store($product_id, "product", $handle);

        if ($product_id) {
          $variant = new Variant;
          $variant->product_id = $product_id;
          $variant->option_1 = '';
          $variant->option_2 = '';
          $variant->option_3 = '';
          $variant->option_4 = '';
          $variant->option_5 = '';
          $variant->option_6 = '';
          $variant->weight = 0;
          $variant->sku_code = $sku;
          $variant->barcode = '';
          $variant->note = '';
          $variant->title = 'Default title';
          $variant->price = $price;
          $variant->sale_id = null;
          $variant->price_compare = 0;
          $variant->stock_quant = 1;
          $variant->status = 'active';
          $variant->created_at = date('Y-m-d H:i:s');
          $variant->updated_at = date('Y-m-d H:i:s');
          $variant->save();

          $data = new CollectionProduct;
          $data->collection_id = $collection_id;
          $data->product_id = $product_id;
          $data->priority = 1;
          $data->created_at = date('Y-m-d H:i:s');
          $data->updated_at = date('Y-m-d H:i:s');
          $data->save();

          foreach ($photos as $photo) {
            Image::store($photo, 'product', $product_id);
          }
        }
      }
    }

    public function importArticles() {
      $article_path = dirname(__FILE__) . '/import/articles.json';
      $articles = file_get_contents($article_path);
      $articles = json_decode($articles, true);

      error_log('hello');

      foreach ($articles as $article) {

        error_log($article['title'] . ' - ' . $article['view']);

        $view = $article['view'];

        if ($article['type'] == 'bai-viet') {
          $item = new Page;
          $item->title = $article['title'];
          $item->image = $article['photo'] ?: '';
          $item->description = $article['description'] ?: '';
          $item->content = $article['content'] ?: '';
          $item->status = 'active';
          $item->template = '';
          $item->view = $view;
          $item->created_at = date('Y-m-d H:i:s');
          $item->updated_at = date('Y-m-d H:i:s');

          $item->tags = '';
          $item->save();

          $slug = $article['slug'];
          Slug::store($item->id, "page", $slug);

        } else {
          $item = Article::create([
            "title" => $article['title'],
            "image" => $article["photo"] ?: '',
            "description" => $article["description"] ?: '',
            "content" => $article["content"] ?: '',
            "priority" => 1000,
            "status" => 'active',
            "raw_text" => '',
            "publish_date" => date('Y-m-d H:i:s'),
            "tags" => '',
            "author" => '',
            "template" => '',
            "view" => $view,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
          ]);

          $article_id = $item->id;
          $slug = $article['slug'];
          Slug::store($article_id, "article", $slug);
        }
      }
    }

    public function importPhotos() {
      ini_set('max_execution_time', '0');
      $path = ROOT . '/public/uploads/';
      $originPath = ROOT . '/public/uploads/origin/';

      $photo_path = dirname(__FILE__) . '/import/products';
      $files = array_diff(scandir($photo_path), array('.', '..'));

      $total = count($files);
      global $size;
      global $quantity;

      foreach ($files as $file) {
        $tmp_name = $photo_path . '/' . $file;
        $newName = $file;
        $newFilePath = $path . $newName;

        error_log('tmp_name: ' . $tmp_name);

        if (moveAndReduceSize($tmp_name, $newFilePath, $quantity)) {
          for ($j = 0; $j < count($size); $j++) {
            moveAndReduceSize($tmp_name, $newFilePath, $quantity, $size[$j]);
          }
          copy($newFilePath, $originPath . $newName);
        }
      }

      echo "DONE";

    }
}


class CustomStoreFrontController extends Controller
{

  public function viewMoreBlog(Request $request, Response $response) {
    $blog_id = isset($_GET['id']) ? $_GET['id'] : null;
    $terms = isset($_GET['terms']) ? $_GET['terms'] : null;
    $page = isset($_GET['page']) ? $_GET['page'] : 2;
    $perpage = isset($_GET['perpage']) ? $_GET['perpage'] : 12;
    $skip = ($page - 1) * $perpage;

    $query = Article::where('article.status', 'active');
    if ($blog_id) {
      $query = $query->join('blog_article', 'blog_article.article_id', '=', 'article.id')
              ->where('blog_article.blog_id', $blog_id);
    }

    if ($terms) {
      if ($_SESSION['lang'] == 'vi') {
        $query = $query->where('article.title', 'LIKE', '%'.$q.'%');
      } else {
        $query = $query->join('article_translations', 'article_translations.article_id', '=', 'article.id')
                ->where('lang', $_SESSION['lang'])
                ->where('article_translations.title', 'LIKE', '%'.$q.'%');
      }
    }

    $articles = $query->skip($skip)->take($perpage)
                ->orderBy('article.id', 'desc')
                ->select('article.*')
                ->get();

    Slug::addHandleToObj($articles, "article");

    foreach ($articles as $key => $article) {
      $blogs = Article::where('blog.status', 'active')
      ->join('blog_article', 'article.id', '=', 'blog_article.article_id')
      ->join('blog', 'blog_article.blog_id', '=', 'blog.id')
      ->where('blog_article.article_id', $article->id)
      ->select('blog.id', 'blog.title')
      ->get();

      foreach ($blogs as $key => $blog) {
        $blog->handle = Slug::where(['post_id' => $blog->id,
          'post_type' => 'blog',
          'lang' => $_SESSION['lang']
          ])->first()->handle;
        Slug::addHandleToObj($blog, "blog");
        if ($_SESSION['lang'] != 'vi') translatePost($blog, "blog");
      }
      $article->blogs = $blogs ?: [];
    }

    if ($_SESSION['lang'] != 'vi') {
      foreach($articles as $key => $article) {
        translatePost($article, "article");
      }
    }

    return $this->view->render($response, 'view-more-blog', [
      'articles' => $articles
    ]);

  }

  public function filterCollection(Request $request, Response $response) {
    $params = $request->getQueryParams();
    $page = $params['page'] ?: 1;
    $perpage = $params['perpage'] ?: 8;
    $skip = ($page - 1) * $perpage;

    $option_1 = $params['option_1'];
    $option_2 = $params['option_2'];
    $option_3 = $params['option_3'];
    $collection_id = $params['collection_id'];
    $price_range = $params['price_range'];

    $sortby = $params['sortby'] ?: 'collection_product.priority-desc';

    $query = Product::where('product.status', 'active')->join('collection_product', 'collection_product.product_id', '=', 'product.id');


    if ($collection_id) {
      $collection_ids = explode(';', $collection_id);
      $query = $query->whereIn('collection_product.collection_id', $collection_ids);
    }

    if ($option_1) {
      $list_option_1 = explode(';', $option_1);
      $query = $query->where(function($q) use($list_option_1) {
        foreach($list_option_1 as $item) {
          $q->orWhere('product.option_1', 'like', "%$item%");
        }
      });
    }

    if ($option_2) {
      $list_option_2 = explode(';', $option_2);
      $query = $query->where(function($q) use($list_option_2) {
        foreach($list_option_2 as $item) {
          $q->orWhere('product.option_2', 'like', "%$item%");
        }
      });
    }

    if ($option_3) {
      $list_option_3 = explode(';', $option_3);
      $query = $query->where(function($q) use($list_option_3) {
        foreach($list_option_3 as $item) {
          $q->orWhere('product.option_3', 'like', "%$item%");
        }
      });
    }

    $is_join_variant = false;
    if ($price_range) {
      $is_join_variant = true;
      $prices = explode('-', $price_range);
      $query = $query->join('variant', 'variant.product_id', '=', 'product.id')
              ->where('variant.status', 'active')
              ->whereBetween('variant.price', [$prices[0], $prices[1]]);
    }

    $sortby = explode('-', $sortby);
    if ($sortby[0] != 'collection_product.priority') {
      if (!$is_join_variant) {
        $query = $query->join('variant', 'variant.product_id', '=', 'product.id')->where('variant.status', 'active');
      }
    }

    $products = $query->skip($skip)->take($perpage + 1)->groupBy('product.id')->select('product.*');
    if ($sortby[1] == 'hot') {
      $products = $products->where('tags', 'like', "%HOT%")->orderBy('product.created_at', 'desc')->get();
    } else {
      $products = $products->orderBy($sortby[0], $sortby[1])->get();
    }

    Slug::addHandleToObj($products, "product");

    if ($_SESSION['lang'] != 'vi') {
      foreach($products as $key => $product) {
        translatePost($product, "product");
      }
    }

    foreach($products as $key => $product) {
      $product->variants = Variant::where('product_id', $product->id)->where('status', 'active')->orderBy('id', 'desc')->get();
      foreach ($product->variants as $key => $variant) {
        $product->variants[$key]['images'] = [];
        $images = Image::where('type', 'variant')->where('type_id', $variant->id)->get();
        if ($images && count($images)) {
          $product->variants[$key]['images'] = $images;
        }
      }

      if ($product->tags) {
        $product->tags = substr($product->tags, 1, strlen($product->tags) - 2);
        $product->tags = explode("#", $product->tags);
      }

    }

    $template = $params['view'] ?: 'view-more-collection';
    return $this->view->render($response, $template, [
      'products' => $products
    ]);
  }

  public function viewMoreCollection(Request $request, Response $response) {
    $page = isset($_GET['page']) ? $_GET['page'] : 2;
    $perpage = isset($_GET['perpage']) ? $_GET['perpage'] : 8;
    $skip = ($page - 1) * $perpage;

    $collection_id = isset($_GET['id']) ? $_GET['id'] : null;

    $query = Product::join('collection_product', 'collection_product.product_id', '=', 'product.id')
                ->where('product.status', 'active');
    if ($collection_id) {
      $query = $query->where('collection_product.collection_id', $collection_id);
    }


    $products = $query->skip($skip)->take($perpage + 1)
                ->select('product.*')
                ->orderBy('collection_product.priority', 'desc')
                ->get();

    Slug::addHandleToObj($products, "product");

    if ($_SESSION['lang'] != 'vi') {
      foreach($products as $key => $product) {
        translatePost($product, "product");
      }
    }

    foreach($products as $key => $product) {
      $product->variants = Variant::where('product_id', $product->id)->where('status', 'active')->orderBy('id', 'desc')->get();
      foreach ($product->variants as $key => $variant) {
        $product->variants[$key]['images'] = [];
        $images = Image::where('type', 'variant')->where('type_id', $variant->id)->get();
        if ($images && count($images)) {
          $product->variants[$key]['images'] = $images;
        }
      }
    }

    $template = isset($_GET['view']) && $_GET['view'] ? $_GET['view'] : 'view-more-collection';
    return $this->view->render($response, $template, [
      'products' => $products
    ]);

  }

    public function feed(Request $request, Response $response)
      {
        $feed = new \Zelenin\Feed;

        $feed->addChannel('https://laptops.vn/feed');

        // required channel elements
        $feed
            ->addChannelTitle('LAPTOP TRẦN PHÁT')
            ->addChannelLink('https://laptops.vn/')
            ->addChannelDescription('Công Ty TNHH MTV Tin Học Trần Phát được thành lập từ năm 2016 với tên thương hiệu Laptop Trần Phát. Chúng tôi là đơn vị chuyên cung cấp các dòng laptop cũ xách tay đã qua sử dụng được thanh lý từ Mỹ, Nhật Bản, Châu Âu...Các sản phẩm công nghệ thông tin như: máy tính PC, camera, hệ thống phòng cháy chữa cháy, dịch vụ bảo trì văn phòng công ty tại Thành Phố Hồ Chí Minh.');

        // optional channel elements
        $feed
            ->addChannelLanguage('vi-EN')
            ->addChannelCopyright('TRANPHAT © 2019')
            ->addChannelManagingEditor('tuantran@laptops.vn (Tran Minh Tuan)')
            ->addChannelWebMaster('tuantran@laptops.vn (Tran Minh Tuan)')
            ->addChannelPubDate(1590201084) // timestamp/strtotime/DateTime
            ->addChannelLastBuildDate(time()) // timestamp/strtotime/DateTime
            // ->addChannelCategory('Channel category', 'http://example.com/category')
            // ->addChannelCloud('rpc.sys.com', 80, '/RPC2', 'myCloud.rssPleaseNotify', 'xml-rpc')
            // ->addChannelTtl(60) // minutes
            ->addChannelImage('https://laptops.vn/themes/laptops/images/logo_small.png', 'https://laptops.vn/', 140, 50, 'LAPTOP TRANPHAT');
            $feed->addChannelElement('sy:updatePeriod', 'hourly');
            $feed->addChannelElement('sy:updateFrequency', 1);

            // ->addChannelRating('PICS label')
            // ->addChannelTextInput('Title', 'Description', 'Name', 'http://example.com/form.php');
            // ->addChannelSkipHours(array(1, 2, 3))
            // ->addChannelSkipDays(array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'));

        /*
        $feed
            ->addChannelElement('test', 'desc', array('attr1' => 'val1', 'attr2' => 'val2'))
            ->addChannelElementWithSub('testsub', array('attr1' => 'val1', 'attr2' => 'val2'))
            ->addChannelElementWithIdentSub('testidentsub', 'child', array('val1', 'val2'));
        */

        // $articles = Article::where('status', 'active')->orderBy('id', 'desc')->get();
        $articles = Article(1, 500, 'created_at-desc');

        foreach ($articles as $key => $item) {
          $feed->addItem();
          $feed
              ->addItemTitle($item->title)
              ->addItemDescription($item->description)
              ->addItemContent($item->content);

          $feed
              ->addItemLink($item->url)
              ->addItemAuthor('tuantran@laptops.vn (Tran Minh Tuan)')
              // ->addItemCategory('Item category', 'http://example.com/category')
              // ->addItemComments('http://example.com/post1/#comments')
              // ->addItemEnclosure('http://example.com/mp3.mp3', 99999, 'audio/mpeg')
              ->addItemGuid($item->url, true)
              ->addItemPubDate($item->created_at) // timestamp/strtotime/DateTime
              ->addItemSource('LAPTOP TRẦN PHÁT', 'https://laptops.vn/feed');

          // $feed->addItemElement('test', 'desc', array('attr1' => 'val1', 'attr2' => 'val2'));
          // $feed->addItemElementSub('group', [ 'var1' => ['key1' => 'value1' ], 'var2' => ['key2' => 'value2' ]]);
          if ($item->blogs && $item->blogs[0]) {
            $feed->addItemCategory($item->blogs[0]->title, $item->blogs[0]->url);
          }

          if ($item->image) {
            $url = HOST . '/uploads/' . $item->image;
            $path = ROOT . '/public/uploads/' . $item->image;
            list($width, $height) = getimagesize($path);
            $feed->addItemElement('media:content', '', [
              'url' => $url,
              'medium' => 'image',
              'width' => $width,
              'height' => $height
            ]);
          }
        }

        $newResponse = $response->withHeader('Content-type', 'application/xml');
        return $newResponse->write($feed);
      }
}


// TODO: add custom admin routes
function add_custom_admin_routes($app)
{
    $app->post('/import-product-custom', '\CustomAdminController:importProduct');
    $app->get('/api/get-color', '\CustomAdminController:getColor');
    //Color attribute
    $app->get('/create-color', '\CustomAdminController:createColorAttribute');
    $app->post('/update-color', '\CustomAdminController:updateColorAttribute');
    $app->get('/get-order', '\CustomAdminController:listOrder');
    $app->get('/api/get-order/status', '\CustomAdminController:statusOrder');

    $app->get('/api/import-product', '\CustomAdminController:importProducts');
    $app->get('/api/import-collection', '\CustomAdminController:importCollections');
    $app->get('/api/import-article', '\CustomAdminController:importArticles');
    $app->get('/api/import-photo', '\CustomAdminController:importPhotos');

}

// TODO: add custom store front routes
function add_custom_store_front_routes($app)
{
    $app->get('/api/blog/view-more', '\CustomStoreFrontController:viewMoreBlog');
    $app->get('/api/collection/view-more', '\CustomStoreFrontController:viewMoreCollection');
    $app->get('/api/filter-collection', '\CustomStoreFrontController:filterCollection');
}


registerLanguages(['en']);
registerCustomField("Đặc tính nổi bật", "product", "gallery");
registerCustomField("Thông số kỹ thuật", "product", "editor");
registerCustomField("Background form đăng ký", "product", "image");

registerCustomField("Banner bottom", "collection", "image");
registerCustomField("Cách chọn Size", "collection", "image");
registerCustomField("Banner voucher online", "article", "image", "(500x600)");