$(document).ready(function () {

  $(".search-menu a").click(function () {
    $(".search-menu").find(".form-search-inline").slideToggle(100);
  });

  $(".mobile-icon-search").click(function () {
    $(".box-search-mobile").slideToggle(200);
  });

  $(".search-btn").click(function () {
    if ($(".icon-search").hasClass("active-search")) {
      $(".icon-search").removeClass("active-search");
      $(".icon-close").addClass("active-search");
      $(".input-bar").addClass("active-wts");
    } else {
      $(".icon-search").addClass("active-search");
      $(".icon-close").removeClass("active-search");
      $(".input-bar").removeClass("active-wts");
    }
  });
  $(".has-submenu > a").on("click", function (e) {
    var parentli = $(this).closest("li");
    if (parentli.hasClass("opened")) {
      parentli.removeClass("opened");
      parentli.find("> ul.sub-menu").slideUp(400);
    } else {
      parentli.addClass("opened");
      parentli.find("> ul.sub-menu").slideDown(400);
    }
    parentli.siblings("li").removeClass("opened");
    parentli.siblings("li").find(".has-submenu.opened").removeClass("opened");
    parentli.siblings("li").find("ul:visible").slideUp();
  });
  $(".has-submenu-2 > a").on("click", function (e) {
    var parentli = $(this).closest("li");
    if (parentli.hasClass("opened-submenu")) {
      parentli.removeClass("opened-submenu");
      parentli.find("> ul.sub-menu-lv2").slideUp(400);
    } else {
      parentli.addClass("opened-submenu");
      parentli.find("> ul.sub-menu-lv2").slideDown(400);
    }
    parentli.siblings("li").removeClass("opened-submenu");
    parentli
      .siblings("li")
      .find(".has-submenu-2.opened-submenu")
      .removeClass("opened-submenu");
    parentli.siblings("li").find("ul:visible").slideUp();
  });
  $(".register-m > a").on("click", function (e) {
    var parentli = $(this).closest("li");
    if (parentli.hasClass("opened-submenu")) {
      parentli.removeClass("opened-submenu");
      parentli.find("> ul.sub-menu-lv2").slideUp(400);
    } else {
      parentli.addClass("opened-submenu");
      parentli.find("> ul.sub-menu-lv2").slideDown(400);
    }
    parentli.siblings("li").removeClass("opened-submenu");
    parentli
      .siblings("li")
      .find(".has-submenu-2.opened-submenu")
      .removeClass("opened-submenu");
    parentli.siblings("li").find("ul:visible").slideUp();
  });
  $(".mobile-menu-btn").on("click", function () {
    $(".overlay-menu").toggleClass("active");
    $(".y-mobile-menu").toggleClass("show");
    return false;
  });
  $(".mobile-sort-btn").on("click", function () {
    $(".overlay-menu").toggleClass("active");
    $(".y-sort-menu").toggleClass("show");
    return false;
  });

  $(".btn-register-drive").on("click", function () {
    $(".overlay-menu").toggleClass("active");
    $(".y-mobile-register").toggleClass("show");
    return false;
  });

  $('.main-nav').on('click', function () {
    $('.sort-category').slideToggle(280);
  });

  function closeMenu() {
    $(".y-mobile-menu").removeClass("show");
    $(".y-sort-menu").removeClass("show");
    $(".y-mobile-register").removeClass("show");
    $(".overlay-menu").removeClass("active");
  }
  $(".overlay-menu, .m-menu-close").on("click", function () {
    closeMenu();
  });
  if ($(".js-home-slider").length > 0) {
    $(".js-home-slider").owlCarousel({
      loop: true,
      margin: 0,
      autoplay: false,
      responsiveClass: true,
      items: 1,
      dots: true,
      navText: [
        "<i class='fas fa-chevron-left'></i>",
        "<i class='fas fa-chevron-right'></i>",
      ],
      rewindNav: true,
      nav: true,
    });
  }
  if ($(".js-home-slider2").length > 0) {
    $(".js-home-slider2").owlCarousel({
      margin: 0,
      autoplay: true,
      responsiveClass: true,
      items: 1,
      dots: true,
      navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>",
      ],
      rewindNav: true,
      nav: false,
    });
    $(".owl-dot").addClass("custom-dot-slide");
  }
  $(".upload-btn-wrapper .input-upload").on("change", function () {
    let filename = $(this)
      .val()
      .replace(/.*(\/|\\)/, "");
    if (filename != "") {
      $(".upload-btn-wrapper .name-file").html(filename);
    }
  });

  function detectmob() {
    if (
      navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/BlackBerry/i) ||
      navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    } else {
      return false;
    }
  }
  var t = {
    delay: 125,
    overlay: $(".fb-overlay"),
    widget: $(".fb-widget"),
    button: $(".fb-button"),
  };
  setTimeout(function () {
    $("div.fb-livechat").fadeIn();
  }, 8 * t.delay);
  if (!detectmob()) {
    $(".ctrlq").on("click", function (e) {
      e.preventDefault(),
        t.overlay.is(":visible") ?
          (t.overlay.fadeOut(t.delay),
            t.widget.stop().animate({
              bottom: 0,
              opacity: 0,
            },
              2 * t.delay,
              function () {
                $(this).hide("slow"), t.button.show();
              }
            )) :
          t.button.fadeOut("medium", function () {
            t.widget
              .stop()
              .show()
              .animate({
                bottom: "30px",
                opacity: 1,
              },
                2 * t.delay
              ),
              t.overlay.fadeIn(t.delay);
          });
    });
  }
});


//product slider trang product.html
var owlItem = $(".product-work-slider"),
  owlThumb = $(".product-img-slider"),
  flag = false,
  duration = 1000;

if (owlItem.length > 0) {
  owlItem
    .owlCarousel({
      lazyLoad: true,
      autoplay: true,
      autoplayHoverPause: true,
      loop: false,
      rewind: true,
      nav: false,
      navText: [
        '<span class="fa fa-angle-left"></span>',
        '<span class="fa fa-angle-right"></span>',
      ],
      dots: false,
      responsive: {
        0: {
          items: 1,
        },
        481: {
          items: 1,
        },
        768: {
          items: 1,
        },
        1200: {
          items: 1,
        },
      },
    })
    .on("changed.owl.carousel", function (e) {
      if (!flag) {
        flag = true;
        owlThumb.trigger("to.owl.carousel", [e.item.index, duration, true]);
        owlThumb.find(".owl-item").removeClass("current");
        owlThumb.find(".owl-item").eq(e.item.index).addClass("current");
        flag = false;
      }
    });
}
if (owlThumb.length > 0) {
  owlThumb
    .owlCarousel({
      margin: 5,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      loop: false,
      nav: true,
      navText: [
        "<img src='/themes/motophattien/images/prev-product.png'>",
        "<img src='/themes/motophattien/images/next-product.png'>",
      ],
      dots: false,
      responsive: {
        0: {
          items: 3,
          nav: false,
          dots: true,
        },
        481: {
          items: 3,
          nav: false,
          dots: true,
        },
        768: {
          items: 3,
          nav: false,
          dots: true,
        },
        1200: {
          items: 3,
          dots: false,
        },
      },
    })
    .on("click", ".owl-item", function (e) {
      owlItem.trigger("to.owl.carousel", [$(this).index(), duration, true]);
      $(this).siblings().removeClass("current");
      $(this).addClass("current");
      e.preventDefault();
    })
    .on("changed.owl.carousel", function (e) {
      if (!flag) {
        flag = true;
        owlThumb.trigger("to.owl.carousel", [e.item.index, duration, true]);
        owlThumb.find(".owl-item").removeClass("current");
        owlThumb.find(".owl-item").eq(e.item.index).addClass("current");
        flag = false;
      }
    });
}
if ($(".prod-box-slider").length > 0) {
  $(".prod-box-slider").owlCarousel({
    lazyLoad: true,
    loop: false,
    margin: 0,
    responsiveClass: true,
    items: 3,
    dots: false,
    navText: [
      "<img src='/themes/motophattien/images/prev.png'>",
      "<img src='/themes/motophattien/images/next.png'>",
    ],
    rewindNav: true,
    nav: true,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1,
        nav: false,
        dots: true,
      },
      480: {
        items: 2,
        nav: false,
        dots: true,
      },
      768: {
        items: 3,
      },
      1200: {
        items: 3,
      },
    },
  });
  $(".owl-prev").addClass("custom-dot-prev");
  $(".owl-next").addClass("custom-dot-next");
  $(".owl-dots").addClass("custom-dots");
  $(".owl-dot").addClass("custom-dot");
}
if ($(".prod-box-slider2").length > 0) {
  $(".prod-box-slider2").owlCarousel({
    lazyLoad: true,
    loop: false,
    margin: 0,
    responsiveClass: true,
    items: 3,
    dots: false,
    navText: [
      "<img src='/themes/motophattien/images/prev.png'>",
      "<img src='/themes/motophattien/images/next.png'>",
    ],
    rewindNav: true,
    nav: true,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1,
        nav: false,
        dots: true,
      },
      480: {
        items: 2,
        nav: false,
        dots: true,
      },
      768: {
        items: 3,
        nav: false,
      },
      1200: {
        items: 3,
      },
    },
  });
  $(".owl-prev").addClass("custom-dot-prev");
  $(".owl-next").addClass("custom-dot-next");
  $(".owl-dots").addClass("custom-dots");
  $(".owl-dot").addClass("custom-dot");
}
if ($(".prod-type-slider").length > 0) {
  $(".prod-type-slider").owlCarousel({
    lazyLoad: true,
    loop: false,
    margin: 0,
    responsiveClass: true,
    items: 4,
    dots: false,
    navText: [
      "<img src='/themes/motophattien/images/prev.png'>",
      "<img src='/themes/motophattien/images/next.png'>",
    ],
    rewindNav: true,
    nav: true,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1,
        nav: true,
        dots: false,
      },
      480: {
        items: 2,
        nav: true,
        dots: false,
      },
      768: {
        items: 3,
      },
      1200: {
        items: 4,
      },
    },
  });
  $(".owl-prev").addClass("custom-dot-prev");
  $(".owl-next").addClass("custom-dot-next");
  $(".owl-dots").addClass("custom-dots");
  $(".owl-dot").addClass("custom-dot");
}

$(".prod-detail-property ul li a.color").click(function () {
  if (!$(this).hasClass("active")) {
    $(this).parents(".prod-detail-property").find("a.color").removeClass("active");
    $(this).addClass("active");
    var name = $(this).data('name');
    $(this).parents(".prod-detail-property").find('.text-property .text-color').html(name).removeClass('hidden');
  }
});

$(window).on('load', function () {
  $(".prod-detail-property ul li:first-child .color").click();
});

$(".drop-down").hover(function () {
  $(this).find(".sub-menu-custom").toggle();
});

$(".flex-item").hover(function () {
  if ($(".image").hasClass("bg-transition")) {
    $(this).find(".image").removeClass("bg-transition");
  } else {
    $(this).find(".image").addClass("bg-transition");
  }
});

$(".js-scroll-top").on("click", function () {
  $("html,body").animate({
    scrollTop: 0
  }, "slow");
  return false;
});
$(document).scroll(function () {
  var y = $(this).scrollTop();
  if (y > 180) {
    $(".box-header").addClass("menu-header-fixed");
    $(".hidden").fadeIn();
  } else {
    $(".box-header").removeClass("menu-header-fixed");
    $(".hidden").fadeOut();
  }
});
$(".vehicle-item").hover(function () {
  if ($(this).find("a").hasClass("line-custom")) {
    $(this).find("a").removeClass("line-custom");
  } else {
    $(this).find("a").addClass("line-custom");
  }
});
$("ul.sub-menu .sub-menu-item").click(function () {
  var tab_id = $(this).attr("data-id");
  if ($(this).hasClass("show")) {
    $("ul.sub-menu .sub-menu-item").removeClass("show");
    $("ul.sub-menu .sub-menu-item").removeClass("active-menu");
    $("ul.sub-menu .sub-menu-item").addClass("sub-menu-item-bh");
    $("ul.sub-menu .sub-menu-item").addClass("hide-active");
    $("ul.sub-menu .sub-menu-2").removeClass("show");
    $(this).removeClass("show");
    $("#" + tab_id).removeClass("show");
  } else {
    $("ul.sub-menu .sub-menu-item").removeClass("show");
    $("ul.sub-menu .sub-menu-item").removeClass("active-menu");
    $("ul.sub-menu .sub-menu-item").removeClass("hide-active");
    $("ul.sub-menu .sub-menu-item").removeClass("sub-menu-item-bh");
    $("ul.sub-menu .sub-menu-2").removeClass("show");
    $(this).addClass("show");
    $("#" + tab_id).addClass("show");
  }
});
$(".sub-menu-vh").hover(function () {
  var tab_id = $(this).attr("data-id");
  $("ul.sub-menu .sub-menu-item").removeClass("show");
  $("#" + tab_id).removeClass("show");
  $("ul.sub-menu .sub-menu-2").removeClass("show");
  $("ul.sub-menu .sub-menu-item").removeClass("active-menu");
});
$(".sub-menu-item").click(function () {
  if ($(".sub-menu-item").hasClass("active-menu")) {
    $(this).removeClass("active-menu");
  } else {
    $(this).addClass("active-menu");
  }
});
$(".scroll-to-target[href^='#']").on("click", function (scroll_to_target) {
  // scroll_to_target.preventDefault();
  var a = this.hash,
    i = $(a);
  $("html, body")
    .stop()
    .animate({
      scrollTop: i.offset().top - 150,
    },
      900,
      "swing",
      function () { }
    );
});
$(".box-main").hover(function () {
  if ($(".box-main").hasClass("box-img-main")) {
    $(".box-main").removeClass("box-img-main");
  } else {
    $(".box-main").addClass("box-img-main");
  }
});
$(".menu-blog ul li").click(function () {
  $("li").removeClass("active");
  $(this).addClass("active");
});
if ($("#lightgallery").length > 0) {
  $("#lightgallery").lightGallery({
    selector: ".item-gall",
  });
}
$(".prod-box-slider").length > 0 &&
  $(window).width() > 1199 &&
  ($(".prod-box-slider .owl-item.active").eq(1).addClass("box-img-main"),
    $(".prod-box-slider .owl-item.active p").eq(1).addClass("vs-custom"),
    $(".prod-box-slider .owl-item.active .box-img-item").eq(1).addClass("box-img-custom-main"),
    $(".prod-box-slider .owl-item.active p").eq(0).addClass("vs-custom2"),
    $(".prod-box-slider .owl-item.active p").eq(2).addClass("vs-custom2"),
    $(".prod-box-slider").on("changed.owl.carousel", function (e) {
      $(".prod-box-slider .owl-item.active .box-img-item").removeClass("box-img-custom"),
        $(".prod-box-slider .owl-item.active p").removeClass("vs-custom"),
        $(".prod-box-slider .owl-item.active p").removeClass("vs-custom2"),
        $(".prod-box-slider .owl-item.active").removeClass("box-img-main"),
        $(".prod-box-slider .owl-item.active .box-img-item").removeClass("box-img-custom-main"),
        setTimeout(function () {
          $(".prod-box-slider .owl-item.active").eq(1).addClass("box-img-main");
          $(".prod-box-slider .owl-item.active .box-img-item").eq(1).addClass("box-img-custom-main");
          $(".prod-box-slider .owl-item.active .box-img-item").eq(0).addClass("box-img-custom");
          $(".prod-box-slider .owl-item.active .box-img-item").eq(2).addClass("box-img-custom");
          $(".prod-box-slider .owl-item.active p").eq(1).addClass("vs-custom");
          $(".prod-box-slider .owl-item.active p").eq(0).addClass("vs-custom2");
          $(".prod-box-slider .owl-item.active p").eq(2).addClass("vs-custom2");
        }, 200);
    })),
  $(".content-recruit").length > 0 && $(".content-recruit .table-responsive table").addClass("table"),
  $(".upload-btn-wrapper .input-upload").on("change", function () {
    var e = $(this)
      .val()
      .replace(/.*(\/|\\)/, "");
    "" != e && $(".upload-btn-wrapper .name-file").html(e), console.log(e);
  });


$('.region').change(function () {
  var value = $(this).val();
  $('.subregion').removeClass('active-select');
  if (!value) {
    $('.subregion').html('');
    return false;
  }

  $.get('/api/region/' + value + '/listSubRegion', function (res) {
    if (!res.code) {
      var html = '';
      $.each(res.data, function (index, elem) {
        html += '<option value="' + elem.id + '">' + elem.name + '</option>';
      });
      $('.subregion').html(html);
    }
  });

});

$('.box-img').click(function () {
  $(this).find('.checkmark').checked = true;
})

$('#check').click(function () {
  $(this).is(":checked");
})

$('.box-sub-menu').click(function () {
  if ($(this).parent().find('input').is(':checked')) {
    $(this).parent().find('input').prop("checked", true);
  } else {
    $(this).parent().find('input').prop("checked", true);
  }
});

function validatePhone(phone) {
  var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
  if (filter.test(phone)) return true;
  return false;
}
$('.sub-menu-item').click(function () {
  $('.sub-menu-2')[0].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-2')[1].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-2')[2].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-2')[3].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-2')[4].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
})
$('.has-submenu-2').click(function () {
  // $(document).off("scroll");
  $('.sub-menu-lv2')[0].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-lv2')[1].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-lv2')[2].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-lv2')[3].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
  $('.sub-menu-lv2')[4].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "nearest"
  });
})
$('.img-hover').hover(function () {
  if ($('.img-hidden').hasClass('hidden-img')) {
    $('.img-hidden').removeClass('hidden-img');
    $('.img-hv').addClass('hidden-img');
  } else {
    $('.img-hidden').addClass('hidden-img');
    $('.img-hv').removeClass('hidden-img');
  }

})


function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=-99999999;';
}


var url_string = window.location.href;
var url = new URL(url_string);

var utm_source = url.searchParams.get("utm_source");
if (utm_source) setCookie("utm_source", utm_source, 30);

var utm_medium = url.searchParams.get("utm_medium");
if (utm_medium) setCookie("utm_medium", utm_medium, 30);

var utm_campaign = url.searchParams.get("utm_campaign");
if (utm_campaign) setCookie("utm_campaign", utm_campaign, 30);
$(function () {
  var CurrentUrl = document.URL;
  var CurrentUrlEnd = CurrentUrl.split('/').filter(Boolean).pop();

  $(".menu-header li a").each(function () {
    var ThisUrl = $(this).attr('href');
    var ThisUrlEnd = ThisUrl.split('/').filter(Boolean).pop();
    if (ThisUrlEnd == CurrentUrlEnd)
      $(this).addClass('active')
  });
});