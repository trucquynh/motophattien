<?php
  $lang = array();
  $lang['PROMOTION'] = 'Khuyến mãi';
  $lang['ADDRESS'] = 'Địa chỉ';
  $lang['OPEN_HOURS'] = 'Thời gian mở cửa';
  $lang['FREE'] = 'Miễn phí';
  $lang['FREE_CALL'] = 'Miễn phí cước gọi';
  $lang['VIEW_MORE'] = 'Xem thêm';
  $lang['IMAGE'] = 'Hình';
  $lang['NEWS'] = 'tin tức';
  $lang['BACK'] = 'Trở lại';
  $lang['SHARE'] = 'Chia sẻ';
  $lang['RELATED_ARTICLES'] = 'Bài viết liên quan';
  $lang['SEARCH_PRODUCT_PLACEHOLDER'] = 'Bạn cần tìm kiếm sản phẩm?';
  $lang['CONTACT'] = 'Liên hệ';
  $lang['NAME'] = 'Họ tên';
  $lang['PHONE'] = 'Số điện thoại';
  $lang['CONTENT'] = 'Nội dung';
  $lang['SUBMIT'] = 'Gửi ngay';
  $lang['SUCCESS'] = 'Thành công';
  $lang['MESSAGE_SUCCESS_CONTACT'] = 'Moto Phát Tiến đã nhận thông tin của quý khách. Chúng tôi sẽ phản hồi nhanh nhất có thể theo thông tin quý khách đã cung cấp. Chân thành cám ơn sự quan tâm của quý khách đến Honda Moto Phát Tiến';
  $lang['LATEST_INFORMATION_UPDATED_ON'] = 'Quý khách có thể cập nhật những thông tin mới nhất tại';
  $lang['SPECIFICATIONS'] = 'Thông số kỹ thuật';
  $lang['GALLERY'] = 'Thư viện ảnh';
  $lang['PRODUCT'] = 'Sản phẩm';
  $lang['SELECT_SIZE'] = 'Chọn size';
  $lang['CONNECT_WITH_US'] = 'Kết nối cùng chúng tôi';
  $lang['QUOTATION'] = 'Đăng ký báo giá';
  $lang['PRICE_QUOTATION'] = 'Báo Giá Honda Moto Chính Hãng Tại Moto Phát Tiến';
  $lang['REGISTRATION'] = 'Đăng ký';
  $lang['LENGTH'] = 'Chiều dài';
  $lang['WIDTH'] = 'Chiều rộng';
  $lang['SEAT_HEIGHT'] = 'Chiều cao yên';
  $lang['WEIGHT'] = 'Trọng lượng';
  $lang['OUTSTANDING_FEATURE'] = 'Đặc tính nổi bật';
  $lang['OTHER_VEHICLES'] = 'Các dòng xe khác';
  $lang['NO_PRODUCTS_WERE_FOUND'] = 'Không tìm thấy sản phẩm phù hợp';
  $lang['SELECT'] = 'Chọn';
  $lang['ORDER_SUCCESS'] = 'Đặt hàng thành công';
  $lang['SORTBY'] = 'Sắp xếp theo';
  $lang['SORT_PRODUCT'] = 'Sắp xếp sản phẩm';
  $lang['HOT_PRODUCT'] = 'Sản phẩm HOT';
  $lang['PRICE_DESC'] = 'Giá từ cao đến thấp';
  $lang['PRICE_ASC'] = 'Giá từ thấp đến cao';
  $lang['CLEAR_FILTER'] = 'Xóa bộ lọc';
  $lang['APPLY_FILTER'] = 'Áp dụng bộ lọc';
  $lang['SIZECHART'] = 'Cách chọn Size';
  $lang['DATE_TO_TEST'] = 'Ngày lái thử';
  $lang['DO_YOU_HAVE_A2'] = 'Anh/Chị có bằng A2 chưa?';
  $lang['REGISTER_FOR_A_DRIVING_TEST'] = 'Đăng ký lái thử xe';
  $lang['SELECT_A_MOTO_TO_TEST'] = 'Chọn xe muốn lái thử';
  $lang['SELECTED_A_MOTO'] = 'Bạn đã chọn lái thử xe';
  $lang['SELECTED_A_MOTO_PRICE'] = 'Bạn đã chọn báo giá xe';
  $lang['PLEASE_SELECT_A_MOTO'] = 'Vui lòng chọn xe muốn lái thử';
  $lang['PLEASE_SELECT_A_MOTO_PRICE'] = 'Vui lòng chọn xe muốn báo giá';
  $lang['I_AGREE_TO_THE'] = 'Tôi đồng ý với quy định từ Honda Môtô Phát Tiến';
  $lang['TOP_FEATURES'] = 'Đặc tính nổi bật';
  $lang['SEARCH'] = 'Tìm kiếm';
  $lang['SEARCH_RESULTS_FOR_KEYWORDS'] = 'Kết quả tìm kiếm cho từ khóa';
  $lang['LEAVE_YOUR_CONTACT'] = 'Để lại thông tin của bạn để chúng tôi tư vấn đúng nhu cầu và miễn phí';
  $lang['LEAVE_YOUR_CONTACT_TO_GET_PROMOTION'] = 'Để lại thông tin nhận khuyến mãi';
  $lang['BOOK_A_FREE_CONSULTATION_CALL'] = 'Nhận cuộc gọi tư vấn miễn phí từ Moto Phát Tiến';
  $lang['PHONE_NUMBER_IS_INVALID'] = 'Số điện thoại không đúng';

  $lang['CATEGORY'] = 'Danh mục';
  $lang['BRAND'] = 'Thương hiệu';
  $lang['MATERIAL'] = 'Chất liệu';
  $lang['PRODUCT_FILTERS'] = 'Lọc sản phẩm';
  $lang['NO_PRODUCT_FOUND'] = 'Không có sản phẩm nào';
  $lang['PRICE'] = 'Giá';
  $lang['FROM'] = 'Từ';
  $lang['COLOR'] = 'Màu sắc';
  $lang['ORDER'] = 'Đặt hàng';
  $lang['REGION'] = 'Tỉnh/TP';
  $lang['SUBREGION'] = 'Quận/huyện';
  $lang['CONTENT'] = 'Nội dung';
  $lang['RELATED_PRODUCTS'] = 'Các sản phẩm cùng loại';
  $lang['OTHER_PRODUCTS'] = 'Các sản phẩm khác';
  $lang['PLEASE_ENTER_YOUR_INFO_BELOW'] = 'Vui lòng để lại thông tin để hoàn tất đặt hàng';
  $lang['PRODUCT_ORDERING'] = 'Bạn đang đặt hàng sản phẩm';
  $lang['SELECT_VEHICLES'] = 'Chọn dòng xe';

  $lang['PLEASE_SELECT_A_COLOR'] = 'Vui lòng chọn màu sắc';
  $lang['PLEASE_SELECT_A_SIZE'] = 'Vui lòng chọn size';
  $lang['PRODUCT_NOT_FOUND'] = 'Sản phẩm không tồn tại';
  $lang['ERROR'] = 'Lỗi';
  $lang['OUTSTANDING_MOTORBIKE'] = 'Xe nổi bật nhất';
  $lang['YES'] = 'Có rồi';
  $lang['NO'] = 'Chưa có';
  $lang['INVALID_DATE'] = 'Ngày không hợp lệ';
  $lang['CUSTOMER_INFORMATION'] = 'Thông tin khách hàng';
  $lang['PLEASE_SELECT_A_DATE'] = 'Vui lòng chọn ngày';

  $lang['REGISTER_URL'] = '/register';
  $lang['PRODUCT_PRICE_URL'] = '/product-price';
  $lang['CONTACT_SUCCESS_URL'] = '/contact-success';



  $lang['BLOG_ALL_URL'] = '/nhom-bai-viet/tat-ca';
  $lang['SEARCH_URL'] = '/tim-kiem';
  $lang['CUSTOMER_ORDER_URL'] = '/khach-hang/don-hang';

  $lang['ALL_PRODUCTS_URL'] = '/du-an/tat-ca';
  $lang['ALL_ARTICLES_URL'] = '/bai-viet/tat-ca';

  $lang['LOGIN_URL'] = '/dang-nhap';
  $lang['CUSTOMER_URL'] = '/khach-hang';

  $lang['CUSTOMER_DASHBOARD_URL'] = '/khach-hang/tong-quan';
  $lang['CUSTOMER_ORDERS_URL'] = '/khach-hang/danh-sach-don-hang';
  $lang['CUSTOMER_CHANGE_PASSWORD_URL'] = '/khach-hang/doi-mat-khau';
  $lang['CUSTOMER_FORGOT_PASSWORD_URL'] = '/khach-hang/quen-mat-khau';
  $lang['CUSTOMER_CREATE_PASSWORD_URL'] = '/khach-hang/tao-mat-khau';
  $lang['CUSTOMER_EDIT_ACCOUNT_URL'] = '/khach-hang/chinh-sua-tai-khoan';
  $lang['CUSTOMER_WISHLIST_URL'] = '/khach-hang/san-pham-yeu-thich';

  $lang['DASHBOARD_URL'] = '/khach-hang/tong-quan';
  $lang['ORDERS_URL'] = '/khach-hang/danh-sach-don-hang';
  $lang['CHANGE_PASSWORD_URL'] = '/khach-hang/doi-mat-khau';
  $lang['FORGOT_PASSWORD_URL'] = '/khach-hang/quen-mat-khau';
  $lang['CREATE_PASSWORD_URL'] = '/khach-hang/tao-mat-khau';
  $lang['EDIT_ACCOUNT_URL'] = '/khach-hang/chinh-sua-tai-khoan';
  $lang['WISHLIST_URL'] = '/khach-hang/san-pham-yeu-thich';
  $lang['SAVE_POINT_URL'] = '/khach-hang/diem-tich-luy';
  $lang['REFERRAL_URL'] = '/khach-hang/ma-gioi-thieu';

  $lang['CART_URL'] = '/gio-hang';
  $lang['CHECKOUT_URL'] = '/dat-hang';
  $lang['CHECKOUT_LOGIN_URL'] = '/dat-hang/dang-nhap';
  $lang['CHECKOUT_REGISTER_URL'] = '/dat-hang/dang-ky';
  $lang['CHECKOUT_SUCCESS_URL'] = '/dat-hang/thanh-cong';



  $lang['COLLECTION_ALL_TITLE'] = 'Tất cả sản phẩm';
