<?php
  $lang = array();
  $lang['PROMOTION'] = 'Promotion';
  $lang['ADDRESS'] = 'Address';
  $lang['OPEN_HOURS'] = 'Open hours';
  $lang['FREE'] = 'Free';
  $lang['FREE_CALL'] = 'Free call';
  $lang['VIEW_MORE'] = 'View more';
  $lang['IMAGE'] = 'Image';
  $lang['NEWS'] = 'news';
  $lang['BACK'] = 'Back';
  $lang['SHARE'] = 'Share';
  $lang['RELATED_ARTICLES'] = 'Related articles';
  $lang['SEARCH_PRODUCT_PLACEHOLDER'] = 'Search products?';
  $lang['CONTACT'] = 'Contact';
  $lang['NAME'] = 'Name';
  $lang['PHONE'] = 'Phone';
  $lang['CONTENT'] = 'Content';
  $lang['SUBMIT'] = 'Submit';
  $lang['SUCCESS'] = 'Success';
  $lang['MESSAGE_SUCCESS_CONTACT'] = 'Moto Phát Tiến received your contact information. Thank you for your inquiry. We will get back to you as soon as possible';
  $lang['LATEST_INFORMATION_UPDATED_ON'] = 'Latest information updated on';
  $lang['SPECIFICATIONS'] = 'Specifications';
  $lang['GALLERY'] = 'Gallery';
  $lang['PRODUCT'] = 'Product';
  $lang['SELECT_SIZE'] = 'Select size';
  $lang['CONNECT_WITH_US'] = 'Connect with us';
  $lang['QUOTATION'] = 'Quotation';
  $lang['PRICE_QUOTATION'] = 'Price Quotation';
  $lang['REGISTRATION'] = 'Registration';
  $lang['LENGTH'] = 'Length';
  $lang['WIDTH'] = 'Width';
  $lang['SEAT_HEIGHT'] = 'Seat height';
  $lang['WEIGHT'] = 'Weight';
  $lang['OUTSTANDING_FEATURE'] = 'Outstanding featues';
  $lang['OTHER_VEHICLES'] = 'Other vehicles';
  $lang['SEARCH'] = 'Search';
  $lang['SEARCH_RESULTS_FOR_KEYWORDS'] = 'Search results for keywords';

  $lang['CATEGORY'] = 'Category';
  $lang['BRAND'] = 'Brand';
  $lang['MATERIAL'] = 'Material';
  $lang['PRODUCT_FILTERS'] = 'Product filters';
  $lang['NO_PRODUCT_FOUND'] = 'No product found';
  $lang['PRICE'] = 'Price';
  $lang['FROM'] = 'From';
  $lang['COLOR'] = 'Color';
  $lang['ORDER'] = 'Order';
  $lang['REGION'] = 'Region';
  $lang['SUBREGION'] = 'SubRegion';
  $lang['CONTENT'] = 'Content';
  $lang['RELATED_PRODUCTS'] = 'Related products';
  $lang['OTHER_PRODUCTS'] = 'Other products';
  $lang['PLEASE_ENTER_YOUR_INFO_BELOW'] = 'Please enter your information below';
  $lang['PRODUCT_ORDERING'] = 'Product ordering';
  $lang['NO_PRODUCTS_WERE_FOUND'] = 'No products were found matching your selection';
  $lang['SELECT'] = 'Select';
  $lang['ORDER_SUCCESS'] = 'Order success';
  $lang['SORTBY'] = 'Sort by';
  $lang['SORT_PRODUCT'] = 'Sort product';
  $lang['HOT_PRODUCT'] = 'HOT product';
  $lang['PRICE_DESC'] = 'Price: Highest to lowest';
  $lang['PRICE_ASC'] = 'Price Lowest to highest';
  $lang['CLEAR_FILTER'] = 'Clear filter';
  $lang['APPLY_FILTER'] = 'Apply filter';
  $lang['SIZECHART'] = 'Size chart';
  $lang['DATE_TO_TEST'] = 'Date';
  $lang['DO_YOU_HAVE_A2'] = 'Do you have A2 driving licence?';
  $lang['REGISTER_FOR_A_DRIVING_TEST'] = 'Register for a driving test';
  $lang['LEAVE_YOUR_CONTACT'] = 'Please leave your contact information and we will get back to you as soon as possible';
  $lang['LEAVE_YOUR_CONTACT_TO_GET_PROMOTION'] = 'Please leave your contact information to get promotion';
  $lang['BOOK_A_FREE_CONSULTATION_CALL'] = 'Book A Free Consultation Call';
  $lang['PHONE_NUMBER_IS_INVALID'] = 'Phone number is invalid';
  $lang['PLEASE_SELECT_A_DATE'] = 'Please select a date';

  $lang['PLEASE_SELECT_A_COLOR'] = 'Please select a color';
  $lang['PLEASE_SELECT_A_SIZE'] = 'Please select a size';
  $lang['PRODUCT_NOT_FOUND'] = 'Product not found';
  $lang['ERROR'] = 'Error';
  $lang['OUTSTANDING_MOTORBIKE'] = 'Outstanding motorbike';
  $lang['YES'] = 'Yes';
  $lang['NO'] = 'No';
  $lang['INVALID_DATE'] = 'Invalid date';
  $lang['CUSTOMER_INFORMATION'] = 'Customer information';
  $lang['SELECT_A_MOTO_TO_TEST'] = 'Select a moto';
  $lang['SELECTED_A_MOTO'] = 'Selected';
  $lang['SELECTED_A_MOTO_PRICE'] = 'Selected';
  $lang['PLEASE_SELECT_A_MOTO'] = 'Please select a moto';
  $lang['PLEASE_SELECT_A_MOTO_PRICE'] = 'Please select a moto';
  $lang['I_AGREE_TO_THE'] = 'I agree to the terms and conditions of Honda Moto Phat Tien';
  $lang['TOP_FEATURES'] = 'Top features';
  $lang['SELECT_VEHICLES'] = 'Select vehicle';


  $lang['REGISTER_URL'] = '/register';
  $lang['PRODUCT_PRICE_URL'] = '/product-price';
  $lang['CONTACT_SUCCESS_URL'] = '/contact-success';


  $lang['COLLECTION_ALL_URL'] = '/collection/all';
  $lang['BLOG_ALL_URL'] = '/blog/all';
  $lang['SEARCH_URL'] = '/search';
  $lang['CUSTOMER_ORDER_URL'] = '/customer/order';

  $lang['ALL_PRODUCTS_URL'] = '/product/all';
  $lang['ALL_ARTICLES_URL'] = '/article/all';

  $lang['LOGIN_URL'] = '/login';
  $lang['CUSTOMER_URL'] = '/customer';
  $lang['CUSTOMER_DASHBOARD_URL'] = '/customer/dashboard';
  $lang['CUSTOMER_ORDERS_URL'] = '/customer/orders';
  $lang['CUSTOMER_CHANGE_PASSWORD_URL'] = '/customer/change-password';
  $lang['CUSTOMER_FORGOT_PASSWORD_URL'] = '/customer/forgot-password';
  $lang['CUSTOMER_CREATE_PASSWORD_URL'] = '/customer/create-passowrd';
  $lang['CUSTOMER_EDIT_ACCOUNT_URL'] = '/customer/edit-account';
  $lang['CUSTOMER_WISHLIST_URL'] = '/customer/wishlist';

  $lang['DASHBOARD_URL'] = '/customer/dashboard';
  $lang['ORDERS_URL'] = '/customer/orders';
  $lang['CHANGE_PASSWORD_URL'] = '/customer/change-password';
  $lang['FORGOT_PASSWORD_URL'] = '/customer/forgot-password';
  $lang['CREATE_PASSWORD_URL'] = '/customer/create-passowrd';
  $lang['EDIT_ACCOUNT_URL'] = '/customer/edit-account';
  $lang['WISHLIST_URL'] = '/customer/wishlist';
  $lang['SAVE_POINT_URL'] = '/customer/save-point';
  $lang['REFERRAL_URL'] = '/customer/referral';

  $lang['CART_URL'] = '/cart';
  $lang['CHECKOUT_URL'] = '/checkout';
  $lang['CHECKOUT_LOGIN_URL'] = '/checkout/login';
  $lang['CHECKOUT_REGISTER_URL'] = '/checkout/register';
  $lang['CHECKOUT_SUCCESS_URL'] = '/checkout/success';


  $lang['COLLECTION_ALL_TITLE'] = 'All product';
